//
//  p1Tests.swift
//  p1Tests
//
//  Created by Fai White on 18/05/22.
//

import XCTest
@testable import p1

class p1Tests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        // Any test you write for XCTest can be annotated as throws and async.
        // Mark your test throws to produce an unexpected failure when your test encounters an uncaught error.
        // Mark your test async to allow awaiting for asynchronous code to complete. Check the results with assertions afterwards.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func primeNumber(n: Int) throws {
        XCTAssertTrue(isPrime(n: n))
    }
    
    func isPrime(n: Int) -> Bool{
        var numD = 0
        
        for i in 2...n {
            if (n%i == 0) {
                numD += 1
            }
            if numD > 1 {
                // no es primo
                return false
            }
        }
        return true
    }

}
