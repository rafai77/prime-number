//
//  ViewController.swift
//  p1
//
//  Created by Fai White on 18/05/22.
//

import UIKit

class ViewController: UIViewController {
    
    private let textNumber = UITextField()
    private let btnPrime = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView() {
        view.backgroundColor = .systemGray2
        textNumber.translatesAutoresizingMaskIntoConstraints = false
        textNumber.placeholder = "numero"
        
        textNumber.layer.borderWidth = 0.5
        textNumber.layer.borderColor = UIColor.white.cgColor
        
        textNumber.delegate = self

        
        
        view.addSubview(textNumber)
        
        NSLayoutConstraint.activate([
            textNumber.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            textNumber.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0),
            
        ])
        
    }


}

extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
        
        let numbers = CharacterSet(charactersIn:"0123456789")
        let characterSet = CharacterSet(charactersIn: string)
        // valida si es un numero para analizar si es primo
        var numberString = (textField.text ?? "") + string
        if string.count == 0 && range.length > 0 {
            // Back pressed
            _ = numberString.popLast()
        }
        print(numberString)
        if  numbers.isSuperset(of: characterSet) {
            guard let number =  Int(numberString) else {
                textNumber.layer.borderColor = UIColor.white.cgColor
                return true
            }
            if number == 1 || number < 1 {
                textNumber.layer.borderColor = UIColor.red.cgColor
                return true
            }
            var numd = 0
            for i in 2...number {
                
                if (number % i) == 0 {
                    numd += 1
                }
                if numd > 1  {
                    textNumber.layer.borderColor = UIColor.red.cgColor
                    return true
                }
            }
                
                textNumber.layer.borderColor = UIColor.green.cgColor
                return true
        } else {
            return false
        }
        textNumber.layer.borderColor = UIColor.white.cgColor
        return true
       }
}





